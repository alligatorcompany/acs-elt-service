{%- set yaml_metadata -%}
source_model: 'v_raw_stg_sample'
derived_columns:
  sample_id: 'sample_col'
  load_date: 'load_date'
  record_source: '!sample_rsrc'
hashed_columns:
  sample_sk: 'sample_col'
  sample_hashdiff:
    is_hashdiff: true
    columns:
      - 'sample_col'
{%- endset -%}

{% set metadata_dict = fromyaml(yaml_metadata) %}

{% set source_model = metadata_dict['source_model'] %}

{% set derived_columns = metadata_dict['derived_columns'] %}

{% set hashed_columns = metadata_dict['hashed_columns'] %}

WITH staging AS (
{{ dbtvault.stage(include_source_columns=true,
                  source_model=source_model,
                  derived_columns=derived_columns,
                  hashed_columns=hashed_columns,
                  ranked_columns=none) }}
)

SELECT *,
current_timestamp AS insert_timestamp_utc
FROM staging