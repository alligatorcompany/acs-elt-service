{%- set source_model = "v_stg_sample" -%}
{%- set src_pk = "sample_hk" -%}
{%- set src_nk = "sample_col" -%}
{%- set src_ldts = "insert_timestamp_utc" -%}
{%- set src_source = "record_source" -%}

{{ dbtvault.hub(src_pk=src_pk, src_nk=src_nk, src_ldts=src_ldts,
                src_source=src_source, source_model=source_model) }}