DOCKER_BUILDKIT=1 docker build \
    --build-arg VERSION='1.3.0' \
    --build-arg PLUGINS='bigquery' \
    . -t docker_dbt:latest
