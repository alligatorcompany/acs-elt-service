import json
import os
from datetime import datetime, timedelta
from os.path import exists
from typing import Iterable

import pyexasol
from dotenv import load_dotenv

load_dotenv()


def parse_data_for_import(project_dir: str):
    if not exists(project_dir):
        return dict()
    with open(project_dir, "r") as run_results:
        data = json.load(run_results)
        data_for_import = []
        for result in data["results"]:
            for timing in result["timing"]:
                key_prefix = timing["name"]
                result[f"{key_prefix}_start_ts"] = timing.get("started_at")
                result[f"{key_prefix}_end_ts"] = timing.get("completed_at")
            generated_at = datetime.strptime(
                data["metadata"]["generated_at"], "%Y-%m-%dT%H:%M:%S.%fZ"
            )
            data_for_import.append(
                [
                    datetime.utcnow(),
                    data["metadata"]["invocation_id"],
                    os.getenv("DBT_PROJECT"),
                    data["args"],
                    generated_at,
                    generated_at + timedelta(0, int(data["elapsed_time"])),
                    data["elapsed_time"],
                    data["metadata"],
                    result["unique_id"],
                    result,
                ]
            )
    return data_for_import


def import_from(data_iterable: Iterable, target_schema: str, target_table) -> None:
    s = pyexasol.connect(
        dsn=f"{os.getenv('EXASOL_SERVICE_HOST')}:{os.getenv('EXASOL_SERVICE_PORT')}",
        user=os.getenv("DBT_USER"),
        password=os.getenv("DBT_PASS"),
        autocommit=True,
        compression=True,
        encryption=False,
    )
    s.import_from_iterable(data_iterable, (target_schema, target_table))


def main() -> None:
    data_for_import = parse_data_for_import(
        f"{os.getenv('DBT_PROJECTDIR')}/target/run_results.json"
    )
    import_from(data_for_import, "yyy_q3_psa", "psa_run_result_dbt")


if __name__ == "__main__":
    main()
