#!/bin/bash
dbt_cli="$@"
docker run \
    -it --rm --entrypoint "" \
    -v /Users/andreasheitmann/.config/gcloud/:/gcp/ \
    -v /Users/andreasheitmann/project/spqr_monitoring_bi/:/project/ \
    --env DBT_CLI_ARG="$dbt_cli" \
    docker_dbt:latest ./dbt.sh
#docker run \
#    -it --rm --user root \
#    acs-elt-service:latest
